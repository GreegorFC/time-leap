﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace Time_Leap
{
    class SoundManager
    {
        // data
        private SoundEffect e1Sound , e2Sound, e3Sound, rockBreaking;
        private Song L1Music, L2Music, L3Music;

        //constructor 
        public SoundManager()
        {
            L1Music = null;
            L2Music = null;
            L3Music = null;
            e1Sound = null;
            e2Sound = null;
            e3Sound = null;
            rockBreaking = null;
        }
        public void LoadContent(ContentManager content)
        {
            L1Music = content.Load<Song>("Level1Music");
            L2Music = content.Load<Song>("Level2Music");
            L3Music = content.Load<Song>("Level3Music");
            e3Sound = content.Load<SoundEffect>("laserFire");
            rockBreaking = content.Load<SoundEffect>("RockBreak");

        }
        // setters and getters 
        public Song GetL1()
        {
            return L1Music;
        }
        public Song GetL2()
        {
            return L2Music;
        }
        public Song GetL3()
        {
            return L3Music;
        }
        public SoundEffect GetRB()
        {
            return rockBreaking;
        }
        public SoundEffect getE3Sound()
        {
            return e3Sound;
        }
    }
}
