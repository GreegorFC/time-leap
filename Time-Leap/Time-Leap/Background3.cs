﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace Time_Leap
{
    class Background3
    {
        //data
        Texture2D texture;
        Vector2 bgPos1, bgPos2;
        int speed;




        public Background3()
        {
            texture = null;
            bgPos1 = new Vector2(0, 0);
            bgPos2 = new Vector2(-950, 0);
            speed = 3;

        }

        public void LoadContent(ContentManager content)
        {
            texture = content.Load<Texture2D>("background3");

        }


        public void Update(GameTime gameTime)
        {
            //setting speed for background
            bgPos1.X = bgPos1.X + speed;
            bgPos2.X = bgPos2.X + speed;

            //scrolling background repeating
            if (bgPos1.X >= 950)
            {
                bgPos1.X = 0;
                bgPos2.X = -950;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, bgPos1, Color.White);
            spriteBatch.Draw(texture, bgPos2, Color.White);
        }
        //setter 
        public int SetSpeed(int newSpeed)
        {
            speed = newSpeed;
            return newSpeed;
        }
    }
}
