﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;


namespace Time_Leap
{
    class Bullet : Sprite
    {
        //data
        const int MAX_DISTANCE = 2000;

        private bool Visible = false;
       
        Vector2 startPosition;
        Vector2 speed;
        Vector2 direction;
       
        private Rectangle bBox;
        private Texture2D texture;
        
      
        //----------------------------------------------
        public void LoadContent(ContentManager contentManager)
        {
            texture = contentManager.Load<Texture2D>("Laser");
            base.LoadContent(contentManager, "Laser");
            
        }
        //----------------------------------------------
        public void Update(GameTime gameTime)
        {
            
            // set the bounding box for the bullet 
            bBox = new Rectangle((int)GetPosition().X, (int)GetPosition().Y, texture.Height, texture.Width);

            // if the bullets are off the screen turn them invisible
            if (Vector2.Distance(startPosition, GetPosition()) > MAX_DISTANCE)
            {
                Visible = false;
            }

            if (Visible == true)
            {
                base.Update(gameTime, speed, direction);
            }
        }
        //----------------------------------------------

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Visible == true)
            {
                base.Draw(spriteBatch);
            }
        }
        //----------------------------------------------
        // fire class
        public void Fire(Vector2 theStartPosition, Vector2 theSpeed, Vector2 theDirection)
        {
            SetPosition(theStartPosition);
            startPosition = theStartPosition;
            speed = theSpeed;
            direction = theDirection;
            Visible = true;
        }
        //----------------------------------------------
        // setters and getters 
        public void SetIsVisible(bool newVisible)
        {
            Visible = false;
            newVisible = Visible;
           
        }
        public bool GetIsVisible()
        {
             
            return Visible;
        }
        public Rectangle getBBox()
        {
            return bBox;
        }
        public Rectangle SetBBox(Rectangle newBBox)
        {
            bBox = newBBox;
            return newBBox;
        }

    }
}
