﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;


namespace Time_Leap
{
    class Player:Sprite
    {
        //Data
        private List<Bullet> aBullet = new List<Bullet>();
        
        private Rectangle healthRec;
        Texture2D texture, healthTexture;
        private bool visible;
        private Vector2 healthBarPos;
        private Rectangle bBox;
        private int health;
        private Game1 game;
        SoundManager sm = new SoundManager();
        

        ContentManager mContentManager;
        //----------------------------------------------
        //consts
        const string PLAYER_ASSET = "PlayerSprite";
        const string HEALTH_ASSET = "Health";
        const int START_POSITION_X = 125;
        const int START_POSITION_Y = 245;
        const int PLAYER_SPEED = 160;
        const int MOVE_UP = -3;
        const int MOVE_DOWN = 3;
        const int MOVE_RIGHT = 3;
        const int MOVE_LEFT = -3;
        const int HEALTH = 200;
        //----------------------------------------------
        //enum
        enum State
        {
            Moving
        }
        State currentState = State.Moving;

        Vector2 direction = Vector2.Zero;
        Vector2 speed = Vector2.Zero;

        KeyboardState previousKeyboardState;
        // constructor
        public Player()
        {
            healthBarPos = new Vector2(50, 50);
            health = 300;
        }
        //----------------------------------------------
        //load the content needed
        public void LoadContent(ContentManager contentManager)
        {
            mContentManager = contentManager;
            foreach (Bullet bullet in aBullet)
            {
                bullet.LoadContent(contentManager);
            }
            healthTexture = contentManager.Load<Texture2D>("Health Bar");
            texture = contentManager.Load<Texture2D>("PlayerSprite");
            SetPosition(new Vector2(START_POSITION_X, START_POSITION_Y));
            sm.LoadContent(contentManager);
            
            base.LoadContent(contentManager, PLAYER_ASSET );

        }
        //----------------------------------------------
        public void Update(GameTime gameTime, GraphicsDevice graphics)
        {
            // get the keyboard state 
            KeyboardState currentKeyboardState = Keyboard.GetState();

            bBox = new Rectangle((int)GetPosition().X, (int)GetPosition().Y, texture.Height, texture.Width);

            //set rectangle for healthRec
            healthRec = new Rectangle((int)healthBarPos.X, (int)healthBarPos.Y, health, 25);

            // call the update movement and update bullets function
            UpdateMovement(currentKeyboardState,graphics);
            UpdateBullet(gameTime, currentKeyboardState);

            // set the previous keystate to the button that had just been pressed
            previousKeyboardState = currentKeyboardState;

            base.Update(gameTime, speed, direction);
        }
        //----------------------------------------------
        private void UpdateMovement(KeyboardState currentKeyboardState, GraphicsDevice graphics)
        {
            if (currentState == State.Moving)
            {
                speed = Vector2.Zero;
                direction = Vector2.Zero;
                // player movement
                if (currentKeyboardState.IsKeyDown(Keys.D) == true)
                {
                    speed.X = PLAYER_SPEED;
                    direction.X = MOVE_RIGHT;
                }
                else if(currentKeyboardState.IsKeyDown(Keys.A)== true)
                {
                    speed.X = PLAYER_SPEED;
                    direction.X = MOVE_LEFT;
                }

                if (currentKeyboardState.IsKeyDown(Keys.W) == true)
                {
                    speed.Y = PLAYER_SPEED;
                    direction.Y = MOVE_UP;
                }
                else if (currentKeyboardState.IsKeyDown(Keys.S) == true)
                {
                    speed.Y = PLAYER_SPEED;
                    direction.Y = MOVE_DOWN;
                }
                // set player bounds and make sure he cant leave the screen from the left hand side of the screen
                if(GetPosition().X <= 0)
                {
                    position.X =0;
                }
                // set player bounds and make sure he cant leave the screen from the top side of the screen
                if (GetPosition().Y <= 0)
                {
                    position.Y = 0;
                }
                // set player bounds and make sure he cant leave the screen from the bottom side of the screen
                if (GetPosition().Y >= 950 - texture.Height)
                {
                    position.Y = 950 - texture.Height;
                }
            }
        }
        //----------------------------------------------
        // updates the bullets
        private void UpdateBullet(GameTime gameTime, KeyboardState currentKeyboardState)
        {
            foreach (Bullet bullet in aBullet)
            {
                
                bullet.Update(gameTime);
            }
            // if the spacebar is pressed shoot the bullet
            if (currentKeyboardState.IsKeyDown(Keys.Space) == true && previousKeyboardState.IsKeyDown(Keys.Space) == false)
            {
                ShootBullet();
            }

            // if the bullet is not visible remove it from the list 
            for (int i = 0; i < aBullet.Count; i++)
            {
                if (!aBullet[i].GetIsVisible())
                {
                    aBullet.RemoveAt(i);
                    i--;
                }
            }
        }
        //----------------------------------------------
        // shoot bullet 
        private void ShootBullet()
        {
            if (currentState == State.Moving)
            {
                bool createNew = true;
                foreach (Bullet bullet in aBullet)
                {
                    // if the bullet is visible and create new equals false shoot the bullet
                    if (bullet.GetIsVisible())
                    {
                        createNew = false;
                        bullet.Fire(GetPosition() + new Vector2(GetSize().Width / 2, GetSize().Height / 2),
                            new Vector2(200, 0), new Vector2(5, 0));
                        sm.getE3Sound().Play();
                        break;
                    }
                }
                // if create new = true then create a new bullet add it to the list and then fire it 
                if (createNew == true)
                {
                    Bullet bullet = new Bullet();
                    bullet.LoadContent(mContentManager);
                    bullet.Fire(GetPosition() + new Vector2(GetSize().Width / 2, GetSize().Height / 2),
                        new Vector2(200, 200), new Vector2(5, 0));
                    aBullet.Add(bullet); // add bullet to the list
                    sm.getE3Sound().Play(); // play sound effect
                }
            }
        }
        //----------------------------------------------
        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (Bullet bullet in aBullet)
            {
                bullet.Draw(spriteBatch);
            }

            spriteBatch.Draw(healthTexture, healthRec, Color.White);
            base.Draw(spriteBatch);
        }
        //----------------------------------------------
        
            // setters and getters 
        public List<Bullet> GetBulletList()
        {
            return aBullet;
        }

        public List<Bullet> SetBulletList(List<Bullet> newBulletList)
        {
            aBullet = newBulletList;
            return newBulletList;
        }
        public bool SetPVisible(bool newVisible)
        {
            visible = newVisible;
            return newVisible;
        }
        public int SetHealth(int newHealth)
        {
            health = newHealth;
            return newHealth;
        }
        public int getHealth()
        {
            
            return health;
        }
        public void PlayerReset()
        {
            if (GetPosition().X >= 2000 - texture.Height)
            {
                SetPosition(new Vector2(125, 245));
            }
            
        }
        public Rectangle getBBox()
        {
            return bBox;
        }
        public Rectangle SetBBox(Rectangle newBBox)
        {
            bBox = newBBox;
            return newBBox;
        }
    }
}
