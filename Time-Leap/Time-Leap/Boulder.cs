﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;

namespace Time_Leap
{
    class Boulder:Sprite
    {
        //data
        private Texture2D texture;
        
        private Vector2 origin;

        private bool isVisible;
        private int speed;
        Random random = new Random();
        private float randX, randY;
        private Rectangle bBox;

        //constructer
        public Boulder(Texture2D newTexture, Vector2 newPosition)
        {
            position = newPosition;
            texture = newTexture;
            speed = 4;
            isVisible = true;
            randX = random.Next(0, 750);
            randY = random.Next(-600, -50);
           
        }
        // load the content
        public void LoadContent(ContentManager Content)
        {
            texture = Content.Load<Texture2D>("Boulder");
            origin.X = texture.Width / 2;
            origin.Y = texture.Height / 2;
        }

        public void Update(GameTime gameTime)
        {
            // set the bounding box
            bBox = new Rectangle((int)position.X, (int)position.Y, texture.Height, texture.Width);

            position.Y = position.Y + speed;
            if(position.Y >= 950)
            {
                position.Y = -50;
            }
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (isVisible)
            {
                spriteBatch.Draw(texture, position, Color.White);
            }
        }
        // setters and getters 
        public bool GetBVisible()
        {
            return isVisible;
        }
        public bool SetVisible(bool visible)
        {
            isVisible = visible;
            return visible;

        }
        public int GetSpeed()
        {
            return speed;
        }
        public int SetSpeed(int newSpeed)
        {
            speed = newSpeed;
            return newSpeed;
        }
        public Vector2 getOrigin()
        {
            return origin;
        }
        public Vector2 SetOrigin(Vector2 newOrigin)
        {
            origin = newOrigin;
            return newOrigin;
               
        }
        public Rectangle getBBox()
        {
            return bBox;
        }
        public Rectangle SetBBox(Rectangle newBBox)
        {
            bBox = newBBox;
            return newBBox;
        }

    }
}
