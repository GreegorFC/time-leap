﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace Time_Leap
{
    class HUD
    {
        //data
        private int playerScore,screenWidth,ScreenHeight;
        private SpriteFont font;
        private Vector2 playerScorePos;
        private bool showHUD;

        //constructer 
        public HUD()
        {
            playerScore = 0;
            showHUD = true;
            ScreenHeight = 950;
            screenWidth = 1500;
            font = null;
            playerScorePos = new Vector2(screenWidth / 2, 25);
        }

        public void LoadContent(ContentManager content)
        {
            font = content.Load<SpriteFont>("LargeFont");
        }

        public void Update(GameTime gameTime)
        {
            //get keyboard state 
            KeyboardState keyState = Keyboard.GetState();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //if we are shwoing HUD then display HUD 
            if (showHUD)
            {
                spriteBatch.DrawString(font, " Score - "+ playerScore, playerScorePos, Color.Red);
            }
        }
        //setters and getters
        public int GetPScore()
        {
            return playerScore;
        }
        public int SetPScore(int newScore)
        {
            playerScore = newScore;
            return newScore;
        }
        public SpriteFont GetFont()
        {
            return font;
        }

    }
}
