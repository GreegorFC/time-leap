﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Time_Leap
{
    class EnemyBullets: Sprite
    {
        // data.
        private Texture2D texture;
        private Vector2 velocity;
        private bool isVisible = true;
        private Rectangle bBox;

        private bool bVisible;

        // constructor
        public EnemyBullets(Texture2D newTexture)
        {
            texture = newTexture;
            bVisible = false;
        }
        // update function 
        public void Update()
        {
           // set up the bounding box
            bBox = new Rectangle((int)position.X, (int)position.Y, texture.Height, texture.Width);
        }
        
        // draw function
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }
        // setters and getters
        public Vector2 SetEPosition(Vector2 newPosition)
        {
            position = newPosition;
            return newPosition;
        }
        public Vector2 GetEPosition()
        {
            return position;
        }
        public Vector2 SetVelocity(Vector2 newVelocity)
        {
            velocity = newVelocity;
            return newVelocity;
        }
        public Vector2 GetVelocity()
        {
            return velocity;
        }
        
        public bool SetBVisible(bool newVisible)
        {
            bVisible = newVisible;
            return newVisible;
        }
        public bool GetBvisible()
        {
            return bVisible;
        }
        public Rectangle getBBox()
        {
            return bBox;
        }
        public Rectangle SetBBox(Rectangle newBBox)
        {
            bBox = newBBox;
            return newBBox;
        }
    }
}
