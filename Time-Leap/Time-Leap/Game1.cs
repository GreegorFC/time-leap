﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework.Media;

namespace Time_Leap
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        // set up an enum to track the game state to see what state the game is in
        public enum State
        {
            Menu,
            Playing,
            GameOver 
        }

        // data
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Player playerSprite;
        Background bg = new Background();
        Background2 bg2 = new Background2();
        Background3 bg3 = new Background3();
        Texture2D menuImage;
        Texture2D gameOverImage;
        int level;
        HUD hud = new HUD();
        SoundManager sm = new SoundManager();
        List<Enemy> enemies = new List<Enemy>();
        List<Boulder> boulderList = new List<Boulder>();
        Random random = new Random();
        Vector2 playerStartPos = new Vector2(125, 245);
        Vector2 def = new Vector2(0,0);
        Vector2 scorePos = new Vector2(235, 650);
        int defaultHealth;
        int healthDecrease;
        int scoreIncrease;
        int healthIncrease1;
        int healthIncrease2;
        //set up first state
        State gameState = State.Menu;

        // construcor
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            menuImage = null;
            gameOverImage = null;
            level = 1;
            defaultHealth = 300;
            healthDecrease = 20;
            scoreIncrease = 10;
            healthIncrease1 = 100;
            healthIncrease2 = 150;
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            playerSprite = new Player();
            //enemySprite = new Enemy();
            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            graphics.PreferredBackBufferWidth = GraphicsDevice.DisplayMode.Width - 50;
            graphics.PreferredBackBufferHeight = GraphicsDevice.DisplayMode.Height - 100;
            //graphics.IsFullScreen = true;
            graphics.ApplyChanges();
            
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            playerSprite.LoadContent(this.Content);
            bg.LoadContent(Content);
            bg2.LoadContent(Content);
            bg3.LoadContent(Content);
            hud.LoadContent(Content);
            menuImage = Content.Load<Texture2D>("phMenu");
            gameOverImage = Content.Load<Texture2D>("GameOverImage");
            sm.LoadContent(Content);
            
            if(level == 1)
            MediaPlayer.Play(sm.GetL1()); // play level 1 music

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// 
        float spawn = 0f;
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            spawn += (float)gameTime.ElapsedGameTime.TotalSeconds;
            // TODO: Add your update logic here

            switch (gameState)
            {
                // updating playing state 
                case State.Playing:
                    {
                        
                        bg.SetSpeed(3); // set the background speed
                        spawn += (float)gameTime.ElapsedGameTime.TotalSeconds; // set up a spawn that will be used gor the enemies and boulders
                        foreach (Enemy enemy in enemies)
                        {
                            // if an enemy hits the player then set the enemy to invisible and make the player lose 20 health
                            if (enemy.getBBox().Intersects(playerSprite.getBBox()))
                            {
                                enemy.SetEVisible(false);
                                playerSprite.SetHealth(playerSprite.getHealth() - healthDecrease);
                            }

                            for (int i = 0; i < playerSprite.GetBulletList().Count; i++)
                            {
                                // if the player bullet hits the enemy then set the enemy and player bullet to be invisible and add 10 to the player score
                                if (enemy.getBBox().Intersects(playerSprite.GetBulletList()[i].getBBox()))
                                {
                                    enemy.SetEVisible(false);
                                    playerSprite.GetBulletList().ElementAt(i).SetIsVisible (false);
                                    hud.SetPScore( hud.GetPScore() + scoreIncrease);
                                }
                            }

                            for (int i = 0; i < enemy.GetEBulletList().Count; i++)
                            {
                                // if the player gets hit with an enemy bullets then set the enemy bullets to be false and make the player lose 20 health
                                if (playerSprite.getBBox().Intersects(enemy.GetEBulletList()[i].getBBox()))
                                {
                                    
                                    enemy.GetEBulletList().ElementAt(i).SetBVisible (false);
                                    playerSprite.SetHealth(playerSprite.getHealth() - healthDecrease);
                                }

                            }
                            enemy.Update(graphics.GraphicsDevice, gameTime);
                        }
                        //----------------------------------------------

                        foreach (Boulder b in boulderList)
                        {
                            for (int i = 0; i < playerSprite.GetBulletList().Count; i++)
                            {
                                // if the player player bullets hits the boulder set the boulder and the bullet to be invisible 
                                //give the player 10 points and play the sound effect 
                                if (b.getBBox().Intersects(playerSprite.GetBulletList()[i].getBBox()))
                                {
                                    b.SetVisible(false);
                                    playerSprite.GetBulletList().ElementAt(i).SetIsVisible (false);
                                    hud.SetPScore(hud.GetPScore() + scoreIncrease);
                                    sm.GetRB().Play();
                                }

                            }
                            // if the boulder hits the player, set it to be invisible , make the player lose 20 health 
                            //and play the sound effect
                            if (b.getBBox().Intersects(playerSprite.getBBox()))
                            {
                                b.SetVisible(false);
                                playerSprite.SetHealth(playerSprite.getHealth() - healthDecrease);
                                sm.GetRB().Play();
                            }
                            b.Update(gameTime);
                        }

                        //----------------------------------------------

                        //----------------------------------------------

                        // call functions
                        LoadBoulder();
                        LoadEnemies();
                        hud.Update(gameTime);
                        bg.Update(gameTime);
                        bg2.Update(gameTime);
                        bg3.Update(gameTime);

                        playerSprite.Update(gameTime, graphics.GraphicsDevice);

                        //if health = 0 go to game over state 
                        if(playerSprite.getHealth()<= 0)
                        {
                            gameState = State.GameOver;
                            playerSprite.SetHealth (defaultHealth); // reset the players health back to full
                            

                            gameend(); // call the game end function

                        }
                        // if the player reaches the right hand side of the screen
                        if(playerSprite.GetPosition().X > graphics.PreferredBackBufferHeight + 850)
                        {
                            level = level + 1; // increase the level by 1

                            //if the current level is level 2
                            if (level == 2)
                            {

                                playerSprite.PlayerReset(); // call player reset function
                                enemies.Clear();// clear the enemies on the screen from the list
                                LoadEnemies(); // call the load enemies functions
                                boulderList.Clear();// clear the boulders on the screen from the list
                                playerSprite.GetBulletList().Clear(); // clear the players bullets on the screen from the list
                                playerSprite.SetHealth(playerSprite.getHealth() + healthIncrease1); // give the player added health for get to the next level
                                MediaPlayer.Play(sm.GetL2()); // play the level 2 music

                            }
                            //if the current level is level 3
                            if (level == 3)
                            {
                                playerSprite.PlayerReset(); // call player reset function
                                enemies.Clear();// clear the enemies on the screen from the list
                                LoadEnemies(); // call the load enemies functions
                                boulderList.Clear();// clear the boulders on the screen from the list
                                playerSprite.GetBulletList().Clear(); // clear the players bullets on the screen from the list
                                playerSprite.SetHealth(playerSprite.getHealth() + healthIncrease2); // give the player added health for get to the next level
                                MediaPlayer.Play(sm.GetL3()); // play the level 3 music
                            }
                           
                            

                        }
                       // if the player reaches the end oflevel 3 play the game over screen
                        if (level > 3)
                        {
                            gameState = State.GameOver;
                        }
                        

                        break;
                    }
                // updating menu state 
                case State.Menu:
                    {
                        //get keyboard state 
                        KeyboardState keyState = Keyboard.GetState();
                        // if enter is pressed at the start menu, play the game
                        if (keyState.IsKeyDown(Keys.Enter))
                        {
                            gameState = State.Playing;
                        }

                        bg.Update(gameTime); // call the update for the first background
                        bg.SetSpeed(1); // set the speed for the background
                        hud.SetPScore(0); // reset the score 
                        playerSprite.SetPosition(playerStartPos);


                        break;
                    }
                // updating gameOver state 
                case State.GameOver:
                    {
                        // get the keyboard state 
                        KeyboardState keyState = Keyboard.GetState();
                        // if the r key is pressed return is to the main menu
                        if (keyState.IsKeyDown(Keys.R))
                        {
                            gameState = State.Menu;
                            //MediaPlayer.Play(sm.GetL1());
                        }
                        gameend(); // call the game end function
                            break;
                    }
            }
            //----------------------------------------------
           

            base.Update(gameTime);
            
        }
        //----------------------------------------------
       
        //----------------------------------------------
       
        //----------------------------------------------

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            switch (gameState)
            {
                //drawing Playing state
                case State.Playing:
                    {
                        
                        if (level == 1)
                        {
                            bg.Draw(spriteBatch); // draw background 1
                        }

                        if(level == 2)
                        {
                            bg2.Draw(spriteBatch); // draw background 2
                        }
                        if(level == 3)
                        {
                            bg3.Draw(spriteBatch); // draw background 3
                        }
                        // if the level is greater than 3 go to the game over menu
                        if(level > 3)
                        {
                            gameState = State.GameOver;
                        }
                        
                       // call the class function draw methods 
                       // hud.Draw(spriteBatch);
                        playerSprite.Draw(this.spriteBatch);
                        
                        foreach (Boulder b in boulderList)
                        {
                            b.Draw(spriteBatch);
                        }
                        foreach (Enemy enemy in enemies)
                        {
                            enemy.Draw(spriteBatch);
                        }
                        hud.Draw(spriteBatch);
                        break;
                    }
                    //drawing Menu State 
                case State.Menu:
                    {
                        bg.Draw(spriteBatch); // draw background to be used at main menu
                        spriteBatch.Draw(menuImage, def, Color.White); // draw main menu image
                        
                        break;
                    }
                    //Drawing GameOver state
                case State.GameOver:
                    {
                        spriteBatch.Draw(gameOverImage, def, Color.White);// draw game over image
                        spriteBatch.DrawString(hud.GetFont(), "Your Final Score Was - " + hud.GetPScore(), scorePos , Color.Red); // draw the players score
                        break;
                    }
            }

            

            spriteBatch.End();

            base.Draw(gameTime);
        }
        //----------------------------------------------
        // load enemies function 
        public void LoadEnemies()
        {
            // get a random number on the Y Axis between 100 and 400 
            int randY = random.Next(100, 400);

            if (spawn >= 1)
            {
                spawn = 0;
                // if the level is 1 and the amount of enemies on screen is less than 4 then add the level 1 enemies and bullets to the list and spawn them in
                if (level == 1 && enemies.Count() < 4)
                {
                    enemies.Add(new Enemy(Content.Load<Texture2D>("Dino"), new Vector2(2000, randY), Content.Load<Texture2D>("Fireball")));
                    
                }
                // if the level is 2 and the amount of enemies on screen is less than 7 then add the level 2 enemies and bullets to the list and spawn them in
                if (level == 2 && enemies.Count < 7)
                {
                    enemies.Add(new Enemy(Content.Load<Texture2D>("enemy2"), new Vector2(2000, randY), Content.Load<Texture2D>("Missile")));
                }
                // if the level is 1 and the amount of enemies on screen is less than 9 then add the level 3 enemies and bullets to the list and spawn them in
                if (level == 3 && enemies.Count < 9)
                {
                    enemies.Add(new Enemy(Content.Load<Texture2D>("enemySprite"), new Vector2(2000, randY), Content.Load<Texture2D>("Laser")));
                }
            }
            for (int i = 0; i < enemies.Count; i++)
            {
                // if the enemies are invisible remove them for the list
                if (!enemies[i].getVisible())
                {
                    enemies.RemoveAt(i);
                    i--;
                    
                }
            }
        }
        //----------------------------------------------
        // load boulders functions
        public void LoadBoulder()
        {
            // get random coordiantes on the x and y axis 
            int randY = random.Next(-1000, -50);
            int randX = random.Next(350, 1950);

            // if the level is 1 and the amount of boulders on screen is less than 3. add new boulders to the list and spawn them in
            if (level == 1 && boulderList.Count() < 3)
            {
                boulderList.Add(new Boulder(Content.Load<Texture2D>("Boulder"), new Vector2(randX,randY)));
            }
            // if the level is 2 and the amount of boulders on screen is less than 5. add new boulders to the list and spawn them in
            if (level == 2 && boulderList.Count() < 5)
            {
                boulderList.Add(new Boulder(Content.Load<Texture2D>("Boulder"), new Vector2(randX, randY)));
            }
            // if the level is 3 and the amount of boulders on screen is less than 7. add new boulders to the list and spawn them in
            if (level == 3 && boulderList.Count() < 7)
            {
                boulderList.Add(new Boulder(Content.Load<Texture2D>("Boulder"), new Vector2(randX, randY)));
            }
            for (int i = 0; i < boulderList.Count; i++)
            {
                // if the boulders are invisible remove them from the list
                if (!boulderList[i].GetBVisible())
                {
                    boulderList.RemoveAt(i);
                    i--;
                }
            }
        }
        //----------------------------------------------
        // game end function 
        void gameend()
        {
            foreach (Boulder b in boulderList)
            {
                b.SetVisible(false); // set the boulders to invisible
            }

            
            foreach(Enemy enemy in enemies)
            {
                enemy.SetEVisible(false); // set the enemies to invisible
            }
            playerSprite.SetPVisible(false); // set the player to invisible
            //hud.SetPScore(0); // reset the score 
            level = 1; // set the level back to level 1
            playerSprite.PlayerReset();
            
            MediaPlayer.Stop(); // stop the music from playing
            playerSprite.SetHealth(300); // reset the halth back to the default amount 

        }
        // getter and setters
        public int SetLevel(int newLevel)
        {
            level = newLevel;
            return newLevel;
        }
        public int GetLevel()
        {
            
            return level;
        }

    }
}
