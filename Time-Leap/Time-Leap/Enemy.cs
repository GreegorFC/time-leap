﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Time_Leap
{
    class Enemy:Sprite 
    {
        //Data
        Texture2D texture;
      
        Vector2 velocity;
        private bool isVisible = true;
        
        float shoot = 0;
        Game1 game;
        SoundManager sm = new SoundManager();
        private Rectangle bBox;

        List<EnemyBullets> bullets = new List<EnemyBullets>();
        Texture2D bulletTexture;
        
        

        Random random = new Random();
        int randX, randY;
        //----------------------------------------------
        // constructor
        public Enemy(Texture2D newTexture, Vector2 newPosition, Texture2D newBulletTexture)
        {
            texture = newTexture;
            SetPosition (newPosition);
            bulletTexture = newBulletTexture;

            randY = random.Next(-4,4);
            randX = random.Next(-4, -1);

            velocity = new Vector2(randX, randY);
        }
        //----------------------------------------------
        public void Update(GraphicsDevice graphics, GameTime gameTime)
        {
            position += velocity;

            // set up the bounding box
            bBox = new Rectangle((int)GetPosition().X, (int)GetPosition().Y, texture.Height, texture.Width);

            // if the enemies reach the top or bottom of the screen then make them bounces off them. 
            if (GetPosition().Y <= 0 || GetPosition().Y >= graphics.Viewport.Height - texture.Height)
            {
                velocity.Y = -velocity.Y;
            }
            // if the enemies is off screen set it to be invisible
            if(GetPosition().X < 0 - texture.Width)
            {
                isVisible = false;
            }

            shoot += (float)gameTime.ElapsedGameTime.TotalSeconds;
            // if more than 3 seconds shoot a bullet and reset the seconds to 0
            if (shoot > 3)
            {
                shoot = 0;
                ShootBullets();
            }
            UpdateBullets();
        }
        //----------------------------------------------
        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (EnemyBullets bullet in bullets)
            {
                bullet.Draw(spriteBatch);
            }
                spriteBatch.Draw(texture, GetPosition(), Color.White);
        }
        //----------------------------------------------
        public void UpdateBullets()
        {
            foreach(EnemyBullets bullet in bullets)
            {
                bullet.SetEPosition(bullet.GetEPosition() + bullet.GetVelocity());
                // if the bullet is goes off the screen set it to be invisible
                if (bullet.GetEPosition().X < 0)
                {
                    
                    bullet.SetBVisible (false);
                }
                
                bullet.Update();
                
            }
            // if the bullet isnt visible remove it from the list.
            for(int i = 0; i< bullets.Count; i++)
            {
                if (!bullets[i].GetBvisible())
                {
                    bullets.RemoveAt(i);
                    i--;
                }
               
                
            }
            
        }

        public void ShootBullets()
        {
            EnemyBullets newBullet = new EnemyBullets(bulletTexture);
            //Vector2 newBulletVelocity = Vector2.Zero;
            Vector2 tempBulletVelocity = newBullet.GetVelocity(); // a temp variable so i can use the velocity 
            tempBulletVelocity.X = tempBulletVelocity.X - 8f; // sets the x axis velocity 
            newBullet.SetVelocity(tempBulletVelocity); // set the bullet velocity to the temp variables velocity
            //newBullet.SetVelocity(newBulletVelocity);
            newBullet.SetEPosition( new Vector2(GetPosition().X + newBullet.GetVelocity().X, GetPosition().Y + (texture.Height/2) -( bulletTexture.Height/2))); // set the postion of the bullet
            
            newBullet.SetBVisible(true); // set the bullet to be visible

            // if the bullet count is less than 3 then add a bullet to the list.
            if (bullets.Count()< 3)
            {
                bullets.Add(newBullet);
                
            }
        }
        //----------------------------------------------
       
        //----------------------------------------------
        // setters and getters
        public bool SetEVisible(bool visible)
        {
            isVisible = visible;
            return visible;
            
        }

        public bool getVisible()
        {
            return isVisible;
        }

        public List<EnemyBullets> GetEBulletList()
        {
            return bullets;
        }
        public List<EnemyBullets> SetBulletList(List<EnemyBullets> newEBulletList)
        {
            bullets = newEBulletList;
            return newEBulletList;
        }
        
        
        public Rectangle getBBox()
        {
            return bBox;
        }
        public Rectangle SetBBox(Rectangle newBBox)
        {
            bBox = newBBox;
            return newBBox;
        }
    }
}
