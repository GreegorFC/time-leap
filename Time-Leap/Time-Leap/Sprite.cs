﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Time_Leap
{
    class Sprite
    {
        //Data

        public Vector2 position = new Vector2(); //The current position of the Sprite
        private Texture2D spriteTexture; //The texture object used when drawing the sprite
        private string assetName; //The asset name for the Sprite's Texture
        private Rectangle size;  //The Size of the Sprite (with scale applied)
        private float scale = 1.0f; //The amount to increase/decrease the size of the original sprite. 
        
        //When the scale is modified throught he property, the Size of the 
        //sprite is recalculated with the new scale applied.
        public float Scale
        {
            get { return scale; }
            set
            {
                scale = value;
                //Recalculate the Size of the Sprite with the new scale
                size = new Rectangle(0, 0, (int)(spriteTexture.Width * scale), (int)(spriteTexture.Height * scale));
                size.Width = spriteTexture.Width;
                size.Height = spriteTexture.Height;
            }
        }

        //Load the texture for the sprite using the Content Pipeline
        public void LoadContent(ContentManager contentManager, string massetName /*string hassetName*/)
        {
            spriteTexture = contentManager.Load<Texture2D>(massetName);
            //healthTexture = contentManager.Load<Texture2D>(hassetName);
            assetName = massetName;
           // healthName = massetName;
            size = new Rectangle(0, 0, (int)(spriteTexture.Width * Scale), (int)(spriteTexture.Height * scale));
        }

        //Update the Sprite and change it's position based on the passed in speed, direction and elapsed time.
        public void Update(GameTime gameTime, Vector2 speed, Vector2 direction)
        {
            position += direction * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
        }

        //Draw the sprite to the screen
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(spriteTexture, position, new Rectangle(0, 0, spriteTexture.Width, spriteTexture.Height), Color.White, 0.0f, Vector2.Zero, Scale, SpriteEffects.None, 0);
        }
        // ------------------
        // setters and getters 
        public Rectangle GetSize()
        {
            return size;
        }
        public string GetAssetName()
        {
            return assetName;
        }

        public Texture2D GetSpriteTexture()
        {
            return spriteTexture;
        }
        public Vector2 SetPosition(Vector2 newPosition)
        {
            position = newPosition;
            return newPosition;
        }
        public Vector2 GetPosition()
        {
            return position;
        }
       
    }
}

